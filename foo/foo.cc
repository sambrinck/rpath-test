#include "foo/foo.hh"
#include "bar/bar.hh"

#include <iostream>

void Foo::foo() {
    std::cout << __FILE__ << ":" << __LINE__ << std::endl;
    Bar::bar();
}