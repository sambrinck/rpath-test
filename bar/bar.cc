#include "bar/bar.hh"

#include <iostream>

void Bar::bar() {
    std::cout << __FILE__ << ":" << __LINE__ << std::endl;
}