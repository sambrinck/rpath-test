#include "foo/foo.hh"
#include <iostream>

int main(int argc, char const *argv[])
{
    std::cout << __FILE__ << ":" << __LINE__ << std::endl;
    Foo::foo();
    return 0;
}
